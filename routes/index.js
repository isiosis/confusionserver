var express = require("express");
var router = express.Router();
var path = require("path");
var fs = require("fs");

path = path.resolve("./public/index.html");

/* GET home page. */
router.get("/", function (req, res, next) {
  fs.createReadStream(path).pipe(res);
});

module.exports = router;
