const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');

const Favorites = require('../models/favorites');

const favoritesRouter = express.Router();

favoritesRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors,  authenticate.verifyUser ,(req, res, next) => {
    Favorites.find({"user": req.user.id})
    .populate('dishes')
    .populate('user')
    .then((favorites) => {
        res.json(favorites[0]);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.find({user: mongoose.Types.ObjectId(req.user.id)})
    .then((favorites) => {
        if (favorites.length == 0) {
            Favorites.create({
                "user": mongoose.Types.ObjectId(req.user.id),
                "dishes" : mongoose.Types.ObjectId(req.body[0]._id)
            })
            .then((favorite) => {
                if (req.body.length > 1) {
                    for (var i = 1; i < req.body.length; i++ ) {
                       if (!favorite.dishes.includes(req.body[i]._id)){
                            favorite.dishes.push(req.body[i]._id);
                       }
                    }
                    favorite.save()
                    .then((resp) => {
                        res.json(resp);
                    }, (err) => next(err))
                    .catch((err) => next(err));
                }
            }, (err) => next(err))
            .catch((err) => next(err));
        }
        else {
            Favorites.findById(favorites[0]._id)
            .then((favorite) => {
                for (var i = 0; i < req.body.length; i++) {
                    if (!favorite.dishes.includes(req.body[i]._id)) {
                        favorite.dishes.push(req.body[i]._id);
                    }
                }
                favorite.save()
                    .then((resp) => {
                        res.json(resp);
                    }, (err) => next(err))
                    .catch((err) => next(err));
            })
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /Favorites');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.deleteOne({user: mongoose.Types.ObjectId(req.user.id)})
    .then((favorite) => {
        res.json(favorite);
    }, (err) => next(err))
    .catch((err) => next(err));
});

favoritesRouter.route('/:dishId')
.options(cors.corsWithOptions, authenticate.verifyUser, (req, res) => { res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    res.statusCode = 400;
    res.end('Cannot perform get operation on /Favorites/:dishId');
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.find({user: mongoose.Types.ObjectId(req.user.id)})
    .then((favorites) => {
        if (favorites.length == 0) {
            Favorites.create({
                "user": mongoose.Types.ObjectId(req.user.id),
                "dishes" : req.params.dishId
            })
            .then((favorite) => {
                res.json(favorite);
            }, (err) => next(err))
            .catch((err) => next(err));
        }
        else {
            Favorites.findById(favorites[0]._id)
            .then((favorite) => {
                if (!favorite.dishes.includes(req.params.dishId)) {
                    favorite.dishes.push(req.params.dishId);
                }
                favorite.save()
                    .then((resp) => {
                        res.json(resp);
                    }, (err) => next(err))
                    .catch((err) => next(err));
            }, (err) => next(err))
            .catch((err) => next(err));
        }
        
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 400;
    res.end('Cannot perform PUT on /Favorites/:dishId');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.find({user: mongoose.Types.ObjectId(req.user.id)})
    .then((favorites) => {
        Favorites.findById(favorites[0]._id)
            .then((favorite) => {
                if (favorite.dishes.includes(req.params.dishId)) {
                    const index = favorite.dishes.indexOf(req.params.dishId);
                    if (index > -1) {
                        favorite.dishes.splice(index, 1);
                    }
                }
                favorite.save()
                    .then((resp) => {
                        res.json(resp)
                    }, (err) => next(err))
                    .catch((err) => next(err));

            }, (err) => next(err))
            .catch((err) => next(err));
            
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = favoritesRouter;